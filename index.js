/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import navigationScreen from './src/navigation/navigationScreen';
import mainPage from './src/screen/mainPage';

AppRegistry.registerComponent(appName, () => navigationScreen);
