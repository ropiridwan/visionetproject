import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import loginPage from '../screen/loginPage';
import registerScreen from '../screen/registerScreen';
import mainPage from '../screen/mainPage';

const Stack = createNativeStackNavigator();

const navigationScreen = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="loginPage">
        <Stack.Screen
          name="loginPage"
          component={loginPage}
          options={{
            title: 'Login',
            // headerStyle: {backgroundColor: '#87CEEB'},
            headerTitleStyle: {
              color: 'black',
            },
            headerTitleAlign: 'center',
          }}
        />
        <Stack.Screen
          name="registerScreen"
          component={registerScreen}
          options={{
            title: 'Register',
            // headerStyle:{backgroundColor:'#87CEEB'},
            headerTitleStyle: {
              color: 'black',
            },
            headerBackTitleStyle: {
              color: 'black',
            },
            headerTitleAlign: 'center',
          }}
        />
        <Stack.Screen
          name="mainPage"
          component={mainPage}
          options={{
            title: 'To Do List',
            // headerStyle:{backgroundColor:'#87CEEB'},
            headerTitleStyle: {
              color: 'black',
            },
            headerBackTitleStyle: {
              color: 'black',
            },
            headerTitleAlign: 'center',
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default navigationScreen;
