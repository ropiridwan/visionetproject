import React, {useEffect} from 'react';
import {
  TextInput,
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Alert,
  TouchableWithoutFeedback,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SQLite from 'react-native-sqlite-storage';
import Modal from 'react-native-modal';

let db = SQLite.openDatabase(
  {
    name: 'MainDB',
    location: 'default',
  },
  () => {},
  Error => {
    Alert.alert('Error Database', Error.massage);
  },
);

const mainPage = ({navigation}) => {
  const [title, setTitle] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [modalVisible, setModalVisible] = React.useState(false);
  const [titleEdit, setTitleEdit] = React.useState('');
  const [result, setResult] = React.useState([{Description: '', Title: ''}]);

  useEffect(async () => {
    await createTable();
    await readDataTable();
  }, []);

  const createTable = () => {
    db.transaction(tx => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS ' +
          'TodoList ' +
          '(ID INTEGER PRIMARY KEY AUTOINCREMENT, Title TEXT, Description TEXT);',
        [],
        (sqlTxn: SQLTrasaction, res: SQLResultSet) => {
          // Alert.alert('Succes Created Table');
        },
        error => {
          Alert.alert('Error Create List Table', error.massage);
        },
      );
    });
  };

  const setTable = async () => {
    await db.transaction(async tx => {
      await tx.executeSql(
        'INSERT INTO TodoList (Title, Description) VALUES (?,?)',
        [title, description],
        (sqlTxn: SQLTrasaction, res: SQLResultSet) => {
          readDataTable();
          setTitle('');
          setDescription('');
          Alert.alert('Succes Add List');
        },
        error => {
        Alert.alert('Error Add List', error.massage);
        },
      );
    });
  };

  const deleteTable = async text => {
    await db.transaction(async tx => {
      await tx.executeSql(
        'DELETE FROM TodoList where Title=?',
        [text],
        (sqlTxn: SQLTrasaction, res: SQLResultSet) => {
          readDataTable();
          setTitle('');
          setDescription('');
          Alert.alert('Succes Delete List');
        },
        error => {
          Alert.alert('Error Database List', error.massage);
        },
      );
    });
  };

  const openModal = (titleA, descriptionA, titleB) => {
    setTitle(titleA);
    setDescription(descriptionA);
    setTitleEdit(titleB);
    setModalVisible(true);
  };

  const editTable = async text => {
    await db.transaction(async tx => {
      await tx.executeSql(
        'UPDATE TodoList SET Title=?, Description=? WHERE Title=?',
        [title, description, titleEdit],
        (sqlTxn: SQLTrasaction, res: SQLResultSet) => {
          readDataTable();
          setTitle('');
          setDescription('');
          Alert.alert('Succes Update List');
          setModalVisible(false);
        },
        error => {
          Alert.alert('Error Update List', error.massage);
        },
      );
    });
  };

  const readDataTable = async () => {
    db.transaction(async tx => {
      tx.executeSql(
        'SELECT Title, Description FROM TodoList',
        [],
        async (sqlTxn: SQLTrasaction, res: SQLResultSet) => {
          const len = res.rows.length;
          if (len > 0) {
            const tamp = [];
            for (let i = 0; i < len; i++) {
              tamp.push(res.rows.item(i));
            }
            setResult(tamp);
          }
        },
        error => {
          Alert.alert('Error Show List', error.massage);
        },
      );
    });
  };

  const onChangeText = text => {
    setTitle(text);
  };

  const onChangeTextDescription = text => {
    setDescription(text);
  };

  const logoutButton = () => {
    navigation.pop();
  };

  return (
    <>
      <View style={Styles.container}>
        <Text
          style={[Styles.textBold, {alignSelf: 'center', marginVertical: 10}]}>
          To Do List
        </Text>
        {result.map((item, index) => {
          return (
            <View style={Styles.containerItem}>
              <View
                key={index}
                style={[
                  Styles.itemStyling,
                  {flexDirection: 'row', justifyContent: 'space-between'},
                ]}>
                <View style={{flex: 1}}>
                  <Text style={[Styles.textBold, {marginVertical: 0}]}>
                    {item.Title}
                  </Text>
                  <Text style={[Styles.textRegular, {marginVertical: 0}]}>
                    {item.Description}
                  </Text>
                </View>
                <TouchableOpacity
                  style={{alignSelf: 'center'}}
                  onPress={() => {
                    openModal(item.Title, item.Description, item.Title);
                  }}>
                  <Ionicons style={Styles.icon} name="create-outline" />
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                style={{alignSelf: 'center'}}
                onPress={() => {
                  deleteTable(item.Title);
                }}>
                <Ionicons style={Styles.icon} name="trash-outline" />
              </TouchableOpacity>
            </View>
          );
        })}

        <TouchableOpacity
          style={Styles.buttonLogut}
          onPress={() => {
            logoutButton();
          }}>
          <Text
            style={[Styles.textBold, {alignSelf: 'center', marginVertical: 0}]}>
            Logout
          </Text>
        </TouchableOpacity>
      </View>
      <View style={Styles.containerAddItem}>
        <View style={{flex: 1}}>
          <TextInput
            style={Styles.containerAddItemStyling}
            placeholder="Add List New..."
            placeholderTextColor={'black'}
            onChangeText={onChangeText}
            value={title}
          />
          <TextInput
            style={Styles.containerAddItemStyling}
            placeholder="Add List Description New..."
            placeholderTextColor={'black'}
            onChangeText={onChangeTextDescription}
            value={description}
          />
        </View>
        <TouchableOpacity
          style={Styles.buttonSubmit}
          onPress={() => {
            setTable();
          }}>
          <Text
            style={[Styles.textBold, {alignSelf: 'center', marginVertical: 0}]}>
            Add New
          </Text>
        </TouchableOpacity>
      </View>
      <Modal
        style={{backgroundColor: 'rgba(0, 0, 0, 0.5)',
        margin: 0}}
        animationType="fade"
        visible={modalVisible}>
        <View
          style={Styles.containerEditItem}>
            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <Text
            style={[Styles.textBold, { marginVertical: 0}]}>
            Edit Todolist
          </Text>
          <TouchableOpacity
            style={{alignSelf: 'flex-end'}}
            onPress={() => {
              setModalVisible(false);
            }}>
            <Ionicons style={Styles.icon} name="close-circle-outline" />
          </TouchableOpacity>
            </View>
          <View style={{}}>
            <TextInput
              style={Styles.containerAddItemStyling}
              value={title}
              placeholder="Add Task..."
              placeholderTextColor={'black'}
              onChangeText={onChangeText}
            />
            <TextInput
              style={Styles.containerAddItemStyling}
              value={description}
              placeholder="Add Task Description..."
              placeholderTextColor={'black'}
              onChangeText={onChangeTextDescription}
            />
          </View>
          <TouchableOpacity
            style={[Styles.buttonSubmit, {width: 70, marginTop: 16}]}
            onPress={() => {
              editTable();
            }}>
            <Text
              style={[
                Styles.textBold,
                {alignSelf: 'center', marginVertical: 0},
              ]}>
              Edit
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
  },
  textBold: {
    fontSize: 16,
    fontWeight: '700',
    color: 'black',
    marginVertical: 10,
  },
  textRegular: {
    fontSize: 12,
    fontWeight: '700',
    color: 'black',
    marginVertical: 10,
  },
  icon: {fontSize: 25, color: 'red', alignSelf: 'center'},
  containerItem: {
    flexDirection: 'row',
    marginHorizontal: 8,
    justifyContent: 'space-around',
    alignItems: 'center',
    alignContent: 'center',
  },
  itemStyling: {
    borderWidth: 1,
    borderColor: 'black',
    width: '90%',
    paddingHorizontal: 8,
    marginBottom: 10,
    paddingBottom: 8,
  },
  containerAddItem: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 1,
    borderWidth: 1,
    borderColor: 'black',
    justifyContent: 'space-between',
    flex: 1,
    paddingHorizontal: 8,
    paddingBottom: 8,
  },
  containerEditItem: {
    backgroundColor: 'white',
    marginHorizontal: 16,
    paddingHorizontal: 16,
    paddingVertical: 20,
    justifyContent: 'center',
    borderRadius: 25,
    borderColor: 'black',
    borderWidth: 1,
  },
  containerAddItemStyling: {
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 16,
    marginTop: 20,
    paddingVertical: 8,
    color: 'black',
    paddingHorizontal: 8,
  },
  buttonSubmit: {
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 16,
    padding: 4,
    alignSelf: 'center',
    marginLeft: 6,
  },
  buttonLogut: {
    borderWidth: 1,
    borderColor: 'white',
    backgroundColor: 'red',
    borderRadius: 16,
    padding: 8,
    marginHorizontal: 8,
  },
});

export default mainPage;
