import React, {useEffect} from 'react';
import {
  TextInput,
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Alert,
} from 'react-native';
import SQLite from 'react-native-sqlite-storage';

let db = SQLite.openDatabase(
  {
    name: 'MainDB',
    location: 'default',
  },
  () => {},
  Error => {
    Alert.alert('Error Database', Error.massage);
  }, 
);

const registerScreen = ({navigation}) => {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [confirmPassword, setConfirmPassword] = React.useState('');

  useEffect(() => {
    createTable();
  }, []);

  const loginButton = () => {
    navigation.navigate('loginPage');
  };

  const createTable = () => {
    db.transaction(tx => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS ' +
          'Users ' +
          '(ID INTEGER PRIMARY KEY AUTOINCREMENT, Email TEXT, Password TEXT);',
        [],
        (sqlTxn: SQLTrasaction, res: SQLResultSet) => {
          // Alert.alert('Succes Created Table');
        },
        error => {
          Alert.alert('Error Create List Table', error.massage);
        },
      );
    });
  };

  const setTable = async () => {
    await db.transaction(async tx => {
      // await tx.executeSql(
      //     "INSERT INTO Users (Email, Password) VALUES ('" + email + "'," + password + ")"
      // );
      await tx.executeSql(
        'INSERT INTO Users (Email, Password) VALUES (?,?)',
        [email, password],
        (sqlTxn: SQLTrasaction, res: SQLResultSet) => {
          Alert.alert('Succes Regist');
        },
        error => {
          Alert.alert('Error Database Regist List', error.massage);
        },
      );
    });
    loginButton();
  };

  const dataDisabled = email === '' && password === '' ? true : false;
  return (
    <View style={Styles.container}>
      <Text style={Styles.textBold}>USER Regist</Text>
      <Text style={Styles.textRegular}>Email</Text>
      <View style={Styles.containerTouchable}>
        <TextInput
          onChangeText={setEmail}
          style={{color: 'black', padding: 10}}
          value={email}
          placeholder="Masukan Email"
        />
      </View>
      <Text style={Styles.textRegular}>Password</Text>
      <View style={Styles.containerTouchable}>
        <TextInput
          onChangeText={setPassword}
          style={{color: 'black', padding: 10}}
          value={password}
          placeholder="Masukan Password"
          secureTextEntry={true}
        />
      </View>
      <Text style={Styles.textRegular}>Confirm Password</Text>
      <View style={Styles.containerTouchable}>
        <TextInput
          onChangeText={setConfirmPassword}
          style={{color: 'black', padding: 10}}
          value={confirmPassword}
          placeholder="Confirm Password"
          secureTextEntry={true}
        />
      </View>
      <TouchableOpacity
        disabled={dataDisabled}
        onPress={setTable}
        style={[
          Styles.containerTouchable,
          {
            backgroundColor: dataDisabled ? 'gray' : '#87CEEB',
            marginTop: 20,
            paddingVertical: 8,
          },
        ]}>
        <Text
          style={[Styles.textBold, {alignSelf: 'center', marginVertical: 0}]}>
          Confirm
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBold: {
    fontSize: 16,
    fontWeight: '700',
    color: 'black',
    marginVertical: 10,
  },
  textRegular: {
    fontSize: 12,
    fontWeight: '700',
    color: 'black',
    marginVertical: 10,
  },
  containerTouchable: {
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 16,
    width: 150,
    height: 40,
  },
});

export default registerScreen;
