import React, {useEffect} from 'react';
import {
  TextInput,
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Alert,
} from 'react-native';
import SQLite from 'react-native-sqlite-storage';

let db = SQLite.openDatabase(
  {
    name: 'MainDB',
    location: 'default',
  },
  () => {},
  Error => {
    Alert.alert('Error Database', Error.massage);
  },
);

const loginPage = ({navigation}) => {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [result, setResult] = React.useState(false);

  useEffect(() => {
    // readDataTable();
  }, []);

  const readDataTable = async () => {
    db.transaction( async tx => {
      tx.executeSql(
        'SELECT Email, Password FROM Users',
        [],
        async (sqlTxn: SQLTrasaction, res: SQLResultSet) => {
          const len = res.rows.length;
          if (len > 0) {
            const tamp = [];
            for (let i = 0; i < len; i++) {
              tamp.push(res.rows.item(i));
            }
            const datajob = tamp.filter(function (item) {
              return item.Email == email && item.Password == password;
            });

            if(datajob.length === 0) {
              // setResult(true)
              Alert.alert('Username atau Password salah !');
            }else{
              await setEmail('')
              await setPassword('')
              await Alert.alert('Berhasil Login !');
              await loginButton()
            }
          }
        },
        error => {
          Alert.alert('Error Login', error.massage);
        },
      );
    });
  };
  const loginButton = () => {
    navigation.navigate('mainPage');
  };

  const registButton = () => {
    navigation.navigate('registerScreen');
  };

  const dataDisabled = email === ''? true :false
  const dataDisabled2 = password === ''? true :false
  const dataValid = dataDisabled && dataDisabled2 ? true : false
  return (
    <View style={Styles.container}>
      <Text style={Styles.textBold}>USER LOGIN</Text>
      <Text style={Styles.textRegular}>Email</Text>
      <View style={Styles.containerTouchable}>
        <TextInput
          onChangeText={setEmail}
          style={{color: 'black', padding: 10}}
          value={email}
          placeholder="Masukan Email"
        />
      </View>
      <Text style={Styles.textRegular}>Password</Text>
      <View style={Styles.containerTouchable}>
        <TextInput
          onChangeText={setPassword}
          style={{color: 'black', padding: 10}}
          value={password}
          placeholder="Masukan Password"
          secureTextEntry={true}
        />
      </View>
      <TouchableOpacity
        disabled={dataValid}
        onPress={readDataTable}
        style={[
          Styles.containerTouchable,
          {
            backgroundColor: dataValid ? 'gray' : '#87CEEB',
            marginTop: 20,
            paddingVertical: 8,
          },
        ]}>
        <Text
          style={[Styles.textBold, {alignSelf: 'center', marginVertical: 0}]}>
          Login
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        // disabled={dataDisabled}
        onPress={registButton}
        // style={[
        //   Styles.containerTouchable,
        //   {
        //     backgroundColor: '#87CEEB',
        //     marginTop: 20,
        //     paddingVertical: 8,
        //   },
        // ]}
      >
        <Text
          style={[
            Styles.textBold,
            {
              alignSelf: 'center',
              marginVertical: 0,
              color: 'blue',
              borderBottomWidth: 1,
              borderBottomColor: 'blue',
              marginTop: 10,
            },
          ]}>
          Belum punya akun ? daftar sekarang !
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBold: {
    fontSize: 16,
    fontWeight: '700',
    color: 'black',
    marginVertical: 10,
  },
  textRegular: {
    fontSize: 12,
    fontWeight: '700',
    color: 'black',
    marginVertical: 10,
  },
  containerTouchable: {
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 16,
    width: 150,
    height: 40,
  },
});

export default loginPage;
